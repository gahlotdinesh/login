import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashoardPage } from './dashoard.page';

describe('DashoardPage', () => {
  let component: DashoardPage;
  let fixture: ComponentFixture<DashoardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashoardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashoardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
