import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './../../services/authentication.service';

@Component({
  selector: 'app-dashoard',
  templateUrl: './dashoard.page.html',
  styleUrls: ['./dashoard.page.scss'],
})
export class DashoardPage implements OnInit {

  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
  }

}
